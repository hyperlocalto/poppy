const mongoose = require('mongoose')
const PopSchema = require('./schemas/PopSchema')

const Pop = new mongoose.model('Pop', PopSchema) // eslint-disable-line

module.exports = { Pop: Pop }
