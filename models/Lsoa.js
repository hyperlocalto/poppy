const mongoose = require('mongoose')
const LsoaSchema = require('./schemas/LsoaSchema')

const Lsoa = new mongoose.model('Lsoa', LsoaSchema) // eslint-disable-line

module.exports = { Lsoa: Lsoa }
