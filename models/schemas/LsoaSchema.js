const mongoose = require('mongoose')

const LsoaSchema = new mongoose.Schema({
  type: String,
  properties: Object,
  geometry: Object
})

module.exports = LsoaSchema
