const mongoose = require('mongoose')

const PopSchema = new mongoose.Schema({
  code: String,
  name: String,
  population: Number,
  area: Number
})

module.exports = PopSchema
