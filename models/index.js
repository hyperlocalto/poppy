const mongoose = require('mongoose')
const { Lsoa } = require('./Lsoa')
const { Pop } = require('./Pop')

const connectDb = () => {
  return mongoose.connect(process.env.DBCONNECTIONSTRING, { useNewUrlParser: true, useFindAndModify: false })
}

const models = { Lsoa, Pop }

module.exports = { ...models, connectDb }
