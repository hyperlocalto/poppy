const { Pop } = require('../models')
const LocationService = require('./location')

module.exports = class PopulationService {
  static async getByLatLon (lat, lon) {
    const lsoa = await LocationService.getLsoaCode(lat, lon)
    const pop = await Pop.findOne({ code: lsoa.properties.LSOA11CD })

    return {
      name: pop.name,
      code: pop.code,
      population: pop.population,
      area: pop.area,
      density: pop.population / pop.area
    }
  }
}
