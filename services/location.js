const { Lsoa } = require('../models')

module.exports = class LocationService {
  static async getLsoaCode (lat, lon) {
    const lsoa = await Lsoa.findOne({
      geometry: {
        $geoIntersects: {
          $geometry: {
            type: 'Point',
            coordinates: [
              parseFloat(lon),
              parseFloat(lat)
            ]
          }
        }
      }
    })
    return lsoa
  }
}
