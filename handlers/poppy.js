const { lambdas, httpResponse } = require('micro-aws-lambda')
const PopulationService = require('../services/population')
const models = require('../models/index')

module.exports.getPopulationDensity = lambdas([
  models.connectDb,
  async ({ event }) => {
    const { lat, lon } = event.pathParameters
    const population = await PopulationService.getByLatLon(lat, lon)
    return httpResponse({
      body: JSON.stringify(population)
    })
  }
])
