# 🌸poppy

An open API for population data.

Currently supports England and Wales using LSOA (Lower Layer Super Output Area) population data. Raw data is available in data/2018. 

## 🌍 API 
#### Population Density
`poppy.hyperlocal.to/lat/{lat}/lon/{lon}`
```
{
"name": "Allerdale 006D", 
"code": "E01019088" (LSOA code),
"population": 1880,
"area": 0.97 (km^2), 
"density": 1938.1443298969073 (population per km^2)
}
```

## 🛠️ Stack
 Node.js with [aws-micro-lambda](https://www.npmjs.com/package/micro-aws-lambda) and [serverless](https://www.npmjs.com/package/serverless)

## 📝 Todo
* [X] Deploy it on poppy.hyperlocal.to
* [ ] Add support for Scotland, Ireland & NI
* [ ] Add a docker-compose for a mongo server + ingesting dataset
* [ ] Create roadmap / plan further features
 
 ## License
 
Code licensed under the MIT License: http://opensource.org/licenses/MIT
